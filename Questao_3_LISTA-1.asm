#INCLUDE P16F628A.INC				; diretiva do compilador
; VC(t) = 7T + 10	->	(1 + 2)T + 10  [equacao final do grupo F]


T			EQU		20H
VC			EQU		21H							


			ORG		000H				; definindo a origem
			GOTO 	INICIO				; vai para a main do programa


INICIO:
			CALL	TESTA
			GOTO	FIM					; se C=0 (chama subrotina de fim), se C=1 essa instrucao e pulada
	
			CALL	POLINOMIO			; chama subrotina para calcular o polinomio


TESTA:
			MOVFW	T					; W=T 
			SUBLW	.10					; 10-T, se T <= 10, flag C=1
			BTFSS	STATUS, C			; testa se a flag c esta setada
			RETURN

POLINOMIO:
			BCF		STATUS, C			; limpando os registradores
			MOVFW	T
			RLF		T, F				; realizando shift left para obter 2T
			RLF		T, F				; realizando shift left para obter 4T
			RLF		T, F				; realizando shift left para obter 6T
			ADDWF	T, W				; somando T + 6T, W = 7T
			ADDLW   .10					; W = 7T + 10
			MOVWF	VC					; VC = W, VC = 7T + 10
			RETURN


FIM:		GOTO	FIM
		
			END