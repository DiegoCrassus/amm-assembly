#INCLUDE P16F628A.INC


VALOR		EQU		32H
M1			EQU		20H

			ORG		000H

			MOVLW	031H

			ANDLW	0D5H ; Mascara 1, opera��o AND para zerar os bits: 0, 1, 4.
			IORLW	091H ; Mascara 2, opera��o OR para setar os bits: 2, 3, 6.
			XORLW	044H ; Mascara 3, opera��o XOR para inverter os bits: 5, 7.
			
			MOVWF	VALOR
			MOVLW	0ABH ; Valor do K1

			XORWF	VALOR ; XOR com o VALOR e K1 para verificar se s�o iguais.

			MOVLW	0FFH
			ADDWF	VALOR ; Soma de FFH para verificar se o XOR retorna 0. Sendo 0 s�o iguais, sendo diferente de 0 s�o diferentes.

			BTFSC	STATUS, C ; Verifica se houve um "vai um" na flag Carry, se caso afirmativo zera a posi��o M1.
			CLRF	M1

FIM:		GOTO	FIM

			END