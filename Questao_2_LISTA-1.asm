#INCLUDE P16F628A.INC

FLAGS		EQU		40H
VALOR		EQU		32H

			ORG		000H
		
			CLRF	FLAGS ; Limpa a variavel FLAGS.

			MOVLW	031H ; Defini��o de Valor para VALOR

			ANDLW	0D5H ; Mascara 1, opera��o AND para zerar os bits: 0, 1, 4.
			IORLW	091H ; Mascara 2, opera��o OR para setar os bits: 2, 3, 6.
			XORLW	044H ; Mascara 3 opera��o XOR para inverter os bits: 5, 7.
			
			MOVWF	VALOR
			MOVLW	0F1H ; Definindo o valor do limite superior em W.

			SUBWF	VALOR, W ; Subtra��o de Valor com Limite superior.
			MOVWF	VALOR
			BTFSC	STATUS, C ; Verifica se houve um "vai um" na flag Carry.
			CALL	FLAG	; Caso 1 na flag Carry, executa FLAG.					

FIM:		GOTO	FIM


FLAG:		
			MOVF	FLAGS, W ; Setando o valor do bit 1 para FLAGS.
			IORLW	001H
			MOVWF	FLAGS
			RETURN

			END