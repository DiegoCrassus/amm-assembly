#INCLUDE P16F628A.INC

; a)
; Tabela de valores para onda senoidal dada por y(x) = 127 * sen(x * (pi / 9))

; |	y(x)	| result dec | result HEX |
; | y(0)	| 	 0		 |	00H		  |
; | y(1)	|    43		 |	2BH		  |
; | y(2)	|	 81		 |	51H		  |
; | y(3)	|	 109	 |	6DH		  |
; | y(4)	|	 125	 |	7DH		  |
; | y(5)	|	 125	 |	7DH		  |
; | y(6)	|	 109	 |	6DH		  |
; | y(7)	|	 81		 |	51H		  |
; | y(8)	|	 43		 |	2BH		  |
; | y(9)	|	 0		 |	00H	      |
; | y(10)	|	-43 	 |	D5H		  |	Complemento de 2 de 2BH.
; | y(11)	|	-81 	 |	5FH	 	  |	Complemento de 2 de 51H.
; | y(12)	|	-109	 |	93H		  |	Complemento de 2 de 6DH.
; | y(13)	|	-125	 |	83H		  |	Complemento de 2 de 7DH.
; | y(14)	|	-125	 |	83H	 	  |	Complemento de 2 de 7DH.
; | y(15)	|	-109	 |	93H		  |	Complemento de 2 de 6DH.
; | y(16)	|	-81		 |	5FH		  |	Complemento de 2 de 51H.
; | y(17)	|	-43 	 |  D5H		  |	Complemento de 2 de 2BH.

; CM = 4 / Fcristal = 4 / 1.6Mhz = 0,0025 = 2,5 useg

CONTADOR	EQU		20H
POINTER		EQU		21H
PWM_REG		EQU		50H


			ORG		000H
			
			CLRF	POINTER
			MOVLW	035H
			MOVWF	FSR		; Inicia os valores do ponteiro para preenchimento da memoria.

			MOVLW	012H
			MOVWF	CONTADOR
			
			CALL	LOOP
			
			MOVLW	023H
			MOVWF	FSR		; Reinicio do ponteiro para leitura da memoria.

			MOVLW	012H
			MOVWF	CONTADOR

			CALL GET_PWM
			

FIM:		GOTO	FIM

LOOP:
			MOVF	POINTER, W
			CALL	SEN_X
			
			ADDLW	080H
			CALL	PUT_MEMORY

			INCF	POINTER, F
			DECFSZ	CONTADOR, F
			GOTO	LOOP
			RETURN


GET_PWM:					; Leitura dos valores da memoria para PWD_REG.
			MOVFW 	INDF
			MOVWF	PWM_REG

			INCF	POINTER, F
			INCF	FSR, F
			DECFSZ	CONTADOR, F
			GOTO	GET_PWM
			RETURN

PUT_MEMORY:					; Carrega a memoria na posi��o 35H ate 46H com os valores da tabela.
			MOVWF	INDF
			INCF	FSR, F
		
			RETURN	

SEN_X:						; Tabela com os valores da onda senoidal.
			ADDWF	PCL, F
			RETLW	000H
			RETLW	02BH
			RETLW	051H
			RETLW	06DH
			RETLW	07DH
			RETLW	07DH
			RETLW	06DH
			RETLW	051H
			RETLW	02BH
			RETLW	000H
			RETLW	0D5H
			RETLW	05FH
			RETLW	093H
			RETLW	083H
			RETLW	083H
			RETLW	093H
			RETLW	05FH
			RETLW	0D5H
			RETLW	000H
			RETLW	000H

			END