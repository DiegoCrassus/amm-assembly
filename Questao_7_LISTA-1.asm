#INCLUDE P16F628A.INC


A			EQU		20H
Be			EQU		21H
Ce			EQU		22H


			ORG		000H		; definindo a origem
			GOTO 	FUNCAO		; vai para a main do programa


FUNCAO:
			MOVFW	Ce
			ADDWF	Be, W
			MOVWF	A

			GOTO	CONDICAO
		
CONDICAO:
			MOVLW	.50
			SUBWF	A, W		; W = A - 50, Se A < 50: C -> 0, A = 60: C -> 1, A > 50: C -> 1
			BTFSC	STATUS, C	; Verifica se a flag Carry = 0
			GOTO	OPERACAO_2	; C = 0, (A < 50) vai para subrotina operacao_1
			GOTO	OPERACAO_1	; C = 1, (A >= 50)vai para subrotina operacao_2

OPERACAO_1:
			MOVFW	Ce
			SUBWF	Be, W
			MOVWF	Be
			GOTO	FIM

OPERACAO_2:
			BCF		STATUS, C	; Reset da flag Carry.
			MOVFW	Be
			RLF		Be, F
			MOVF	Be, W
			MOVWf	A
			GOTO	FIM	

FIM:		GOTO	FIM
	
			END				