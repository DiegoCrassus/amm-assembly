#INCLUDE P16F628A.INC
; CM = 4 / Fcristal = 4 / 1.6Mhz = 0,0025 = 2,5 useg

REPET		EQU		10H
CONTADOR	EQU		20H
POINTER		EQU		21H
X			EQU		50H
M			EQU		60H
ALFA		EQU		52H	; AZUL = A.
BETA		EQU		53H	; BOLA = B.
K1			EQU		54H
K2			EQU		55H


			ORG		000H

			MOVLW	0FFH
			MOVWF	REPET
			
			CLRF	POINTER
			MOVLW	035H
			MOVWF	FSR

			MOVLW	012H
			MOVWF	CONTADOR
			
			MOVLW	0ABH
			MOVWF	K1

			MOVLW	067H
			MOVWF	K2

			MOVLW	015H
			MOVWF	X

			MOVLW	05AH
			MOVWF	ALFA

			MOVLW	0A6H
			MOVWF	BETA

			CALL	GET_MEM


			
FIM:		GOTO	FIM

CIFRA:
			XORWF	K1, W	; K1 xor X.
			MOVWF	X
			SWAPF	X, W	; SWAP(X xor K1).
			
			ADDWF	ALFA, W	; SWAP(X xor K1) + A.
			XORWF	K2, W	; SWAP(X xor K1) + A xor K2.
			MOVWF	X
			SWAPF	X, W	; SWAP(SWAP(X xor K1) + A xor K2).

			MOVWF	M		; M = SWAP(SWAP(X xor K1) + A xor K2).

			RETURN

GET_MEM:					
			MOVFW 	INDF

			CALL	CIFRA		
			CALL	ESPERA
			CALL	GET_X

			INCF	POINTER, F ; POINTER INICIA COM EM 35H e vai ate 46H 
			INCF	FSR, F
			DECFSZ	CONTADOR, F ; CONTADOR COME�A EM 12H E DECREMENTA
			GOTO	GET_MEM
			RETURN

GET_X:	
			SWAPF	M, W	; SWAP(M xor K1) + B xor K2
			MOVWF	X
			XORWF	K2, W	; SWAP(M xor K1) + B
			ADDWF	BETA, W	; SWAP(M xor K1)
			MOVWF	M
			SWAPF	M, W	; M xor K1
			XORWF	K1, W

			MOVWF	X
			RETURN


ESPERA:						; TEMPO = 8 * REPET (255) * CM (0,0025) = 5.1s
			NOP
 			NOP
 			NOP
			NOP
			NOP
			DECFSZ	REPET, F
			GOTO	ESPERA
			RETURN

			END